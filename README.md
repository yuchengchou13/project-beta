# CarCar

Team:

* Aurash Zandieh - Service API

* Matthew Chou - Sales API

## How to Run Project
Run these commands in your terminal:

1. docker volume create beta-data
2. docker-compose build
3. docker-compose up

After that here are the following CRUD route documentation for each of the services:

# Sales:

- Customer
    - POST  - http://localhost:8090/api/customers/
    - GET  - http://localhost:8090/api/customers/
    - DELETE - http://localhost:8090/api/customers/id/
- Sales
    - POST  - http://localhost:8090/api/sales/
    - GET  - http://localhost:8090/api/sales/
    - DELETE - http://localhost:8090/api/sales/id/
- Salesperson
    - POST  - http://localhost:8090/api/salespeople/
    - GET  - http://localhost:8090/api/salespeople/
    - DELETE - http://localhost:8090/api/salespeople/id/


# Appointments:

1. POST - http://localhost:8080/api/appointments/
2. GET - http://localhost:8080/api/appointments/


# Automobile Information:

1. DELETE - http://localhost:8100/api/automobiles/VIN#/
2. PUT - http://localhost:8100/api/automobiles/VIN#/
3. POST - http://localhost:8100/api/automobiles/
4. GET - http://localhost:8100/api/automobiles/ (LIST)
5. GET - http://localhost:8100/api/automobiles/VIN#/ (SPECIFIC)

# Vehicle Models:

1. DELETE - http://localhost:8100/api/models/:id/
2. PUT - http://localhost:8100/api/models/1/
3. GET - http://localhost:8100/api/models/1/ (SPECIFIC)
4. POST - http://localhost:8100/api/models/
5. GET - http://localhost:8100/api/models/1/ (LIST)

# Manufacturers:

1. DELETE - http://localhost:8100/api/manufacturers/1/
2. PUT - http://localhost:8100/api/manufacturers/1/
3. GET - http://localhost:8100/api/manufacturers/1/ (SPECIFIC)
4. POST - http://localhost:8100/api/manufacturers/
5. GET - http://localhost:8100/api/manufacturers/1/ (LIST)


For the above mentioned URL paths, you'll need the specific VIN# or ID# to get those sites to work.

AutomibleVO is a value object for sales.

The VIN # is a VO for automobiles.


## Design

## Service microservice

I created an Appointment model and a Technician model. Since you need to be able to input a Technician's name from a dropdown, as well as their ID number. Similarly, the appiontment model is very specific with input fields, with the technician being one of them.

With the inventory available, the service is tied to the automobile's VIN #. Without that being properly set up, it would not be possible to create a successful service application.

## Sales microservice

I created Customer, Sales, SalesPerson, and AutomobileVO models to store relevant data (such as Customer name, Salesperson name, employee id, price of car, which car, etc) within them. The user will be able to create a salesperon, a new customer, a new sale, or see the full list of sales done by specific salesperson or by all the salespeople.


# Link to Exalidraw

1. https://excalidraw.com/#room=2f798c15cb69d801bfbc,PXipqilGAzJN3Zn1I-1wuw
