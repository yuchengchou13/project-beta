import React from 'react';
import { useEffect, useState } from "react";

function SaleList(props) {
    const [sales, setSales] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setSales(data.sales)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);


    return (
        <>
        <a href="http://localhost:3000/sales/new/"><button type="button" className="btn btn-success">Record a new sale</button></a>

        <table className="table table-image">
                <thead>
                    <tr>
                        <th scope="col">Sales Person</th>
                        <th scope="col">Employee Number</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.href}>
                                <td>{ sale.sales_person.name }</td>
                                <td>{ sale.sales_person.employee_number }</td>
                                <td>{ sale.customer.name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>{ sale.price }</td>
                                <td>
                                    {/* <button type="button" className="btn btn-warning" onClick={() => deleteAutomobile(automobile)} >Delete</button> */}
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>


        </>
    )
};
export default SaleList
