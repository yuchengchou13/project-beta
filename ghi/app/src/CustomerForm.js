import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';

function CustomerForm() {

    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [zipCode, setZipCode] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [id, setID] = useState('');
    const navigate = useNavigate();

    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleChangeAddress = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handleChangeCity = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    const handleChangeState = (event) => {
        const value = event.target.value;
        setState(value);
    }

    const handleChangeZipCode = (event) => {
        const value = event.target.value;
        setZipCode(value);
    }

    const handleChangePhoneNumber = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    const handleChangeID = (event) => {
        const value = event.target.value;
        setID(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.name = name;
        data.address = address;
        data.city = city;
        data.state = state;
        data.zip_code = zipCode;
        data.phone_number = phoneNumber;
        // data.id = id;

        const customerUrl = "http://localhost:8090/api/customers/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const customerResponse = await fetch(customerUrl, fetchConfig);

        if(customerResponse.ok) {
            const addCustomer = await customerResponse.json();


            setName('');
            setAddress('');
            setCity('');
            setState('');
            setZipCode('');
            setPhoneNumber('');
            // setID('');
            navigate('/')

        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4 rounded-3">
                    <div className="d-flex mb-3 align-items-center justify-content-center">
                        <h1>Add a Customer</h1>
                    </div>
                    <form onSubmit={handleSubmit} className="row g-3">
                        <div className="col-12 form-floating">
                            <input value={name} onChange={handleChangeName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label className="mx-2" htmlFor="name">Name</label>
                        </div>
                        <div className="col-md-12 form-floating">
                            <input value={address} onChange={handleChangeAddress} placeholder="Address" type="text" name="address" id="address" className="form-control" />
                            <label className="mx-2" htmlFor="address">Address</label>
                        </div>
                        <div className="col-md-12 form-floating">
                            <input value={city} onChange={handleChangeCity} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                            <label className="mx-2" htmlFor="city">City</label>
                        </div>
                        <div className="col-md-6">
                            <div className="form-floating">
                                <input value={state} onChange={handleChangeState} placeholder="State" required type="text" name="state" id="state" className="form-control" />
                                <label htmlFor="state">State (Ex: CA, TX)</label>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-floating">
                                <input value={zipCode} onChange={handleChangeZipCode} placeholder="Zip Code" required type="number" name="zip_code" id="zip_code" className="form-control" />
                                <label htmlFor="zip_code">Zip Code</label>
                            </div>
                        </div>
                        <div className="col-12 form-floating">
                            <input value={phoneNumber} onChange={handleChangePhoneNumber} placeholder="Phone Number" required type="tel" name="phone_number" id="phone_number" className="form-control" />
                            <label className="mx-2" htmlFor="phone_number">Phone Number </label>
                        </div>

                        <div className="d-grid col-md-6 mx-auto">
                            <button className="btn btn-outline-primary">Add Customer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    )
}
export default CustomerForm
