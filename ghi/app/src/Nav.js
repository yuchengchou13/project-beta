import { NavLink, Link } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active me-2" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown me-2">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                 Inventory
              </NavLink>
              <ul className="dropdown-menu dropdown-menu-end">
                <li>
                  <Link to="/manufacturers" className="dropdown-item">List of manufacturers</Link>
                </li>
                <li>
                  <Link to="/manufacturers/new" className="dropdown-item">Add a manufacturer</Link>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li>
                  <Link to="/models" className="dropdown-item">List of vehicle models</Link>
                </li>
                <li>
                  <Link to="/models/new" className="dropdown-item">Add a vehicle model</Link>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li>
                  <Link to="/automobiles" className="dropdown-item">List of automobiles</Link>
                </li>
                <li>
                  <Link to="/automobiles/new" className="dropdown-item">Add an automobile</Link>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown me-2">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </NavLink>
              <ul className="dropdown-menu dropdown-menu-end">
                <li>
                  <Link to="/salespeople/new" className="dropdown-item">Add a sales person</Link>
                </li>
                <li>
                  <Link to="customers/new" className="dropdown-item">Add a customer</Link>
                </li>
                <li>
                  <Link to="/sales/new" className="dropdown-item">Record a sale</Link>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li>
                  <Link to="/sales" className="dropdown-item">List all sales</Link>
                </li>
                <li>
                  <Link to="/salespeople" className="dropdown-item">List all sales by sales person</Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}


export default Nav;
