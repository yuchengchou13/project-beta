import { React, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function VehicleModelForm() {
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [picture, setPicture] = useState('');
    const [manufacturers, setManufacturers] = useState([]);
    const [manufacturer, setManufacturer] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturers(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.manufacturer_id = manufacturer;
        data.picture_url = picture;



        const modelsUrl = "http://localhost:8100/api/models/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelsUrl, fetchConfig);
        console.log(response)

        if (response.ok) {
            const newVehicleModel = await response.json();

            setName('');
            setManufacturer('');
            setPicture('');
            navigate('/models');
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);

        }
    }
    useEffect(() => {
        fetchData();
        }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form onSubmit={handleSubmit} id="create-vehicle_model-form">
                        <div className='form-floating mb-3'>
                            <input onChange={handleNameChange} value={name} placeholder='name' required type="text" name="name" id="name" className='form-control'/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handlePictureChange} value={picture} placeholder='picture' required type="link" name="picture" id="picture" className='form-control'/>
                            <label htmlFor="picture">Picture url</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleManufacturerChange} value={manufacturer} placeholder="manufacturer" name="manufacturer" id="manufacturer" className="form-select">
                                <option value="">Choose a Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return(
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        </div>

    );
}
export default VehicleModelForm;
