import { Link } from 'react-router-dom';
import { React, useState, useEffect } from 'react';

function AppointmentsList(props) {
    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);





    return (
        <>
        <p>
        </p>
        <a href="http://localhost:3000/appointments/new/"><button type="button" className="btn btn-success">Add an appointment</button></a>
        <table className="table table-image">
            <thead>
                <tr>
                    <th scope="col">Vin</th>
                    <th scope="col">Owner</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time</th>
                    <th scope="col">Technician</th>
                    <th scope="col">Reason</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr>
                            <td>{appointment.vin}</td>
                            <td>{appointment.owner}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.technician}</td>
                            <td>{appointment.reason}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>

        </>

    )



//     return (
//         <div>
//             <br />
//             <div className='d-grid gap-5 d-sm-flex justify-content-sm-center'>
//                 <Link to='/service/' className='btn btn-primary btn-lg px-4 gap-3'>Click here to create an Appointment</Link>
//             </div>
//             <br />
//             <table className="table table-striped">
//                 <thead>
//                     <tr>
//                         <th>Name</th>
//                     </tr>
//                 </thead>
//                 <tbody>
//                     {appointments.map(appointment => {
//                         return (
//                             <tr key={ appointment.id }>
//                                 <td>{ appointment.name }</td>
//                                 {/* <td><button onClick={() => ManufacturerDelete(manufacturer)}>Delete</button></td> */}
//                             </tr>
//                         );
//                     })}
//                 </tbody>
//             </table>
//         </div>
//     )
// }




    // const automobiles = props.automobiles || []
    // const [auto, setAuto] = useState([]);

    // const deleteAutomobile = async (automobile) => {

    //     const url = `http://localhost:8100${automobile.href}`
    //     const fetchConfig = {
    //         method: "DELETE",
    //     }

    //     const response = await fetch(url, fetchConfig);
    //     if (response.ok) {
    //         return (
    //             <div className="alert alert-danger" role="alert">This automobile has been deleted</div>
    //         )
    //     }
    // }

    // const fetchData = async () => {

    //     const url = "http://localhost:8100/api/automobiles/"

    //     const response = await fetch(url);

    //     if (response.ok) {
    //         const data = await response.json();
    //         setAuto(data.automobile)
    //     }

    // }


    // useEffect(() => {
    //     fetchData();
    // }, []);



    }

export default AppointmentsList;
