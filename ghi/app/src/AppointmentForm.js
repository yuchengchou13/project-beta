import { React, useEffect, useState } from 'react';


function AppointmentForm() {

    const [vin, setVin] = useState('');
    const [owner, setOwner] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');


    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleOwnerChange = (event) => {
        const value = event.target.value;
        setOwner(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }




    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.owner = owner;
        data.date = date;
        data.time = time;
        data.technician = technician;
        data.reason = reason;

        console.log(data);

        const setAppointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(setAppointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            setVin('');
            setOwner('');
            setDate('');
            setTime('');
            setTechnician('');
            setReason('');
        }
    }


    const fetchData = async () => {
        const url = "http://localhost:8080/api/service/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);




    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create an appointment</h1>
            <form onSubmit={handleSubmit} id="add-appointment-form">
                <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">Vin</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleOwnerChange} value={owner} placeholder="owner" required type="text" name="owner" id="owner" className="form-control" />
                <label htmlFor="owner">Owner</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleDateChange} value={date} placeholder="date" required type="DATE_AND_TIME" name="date" id="date" className="form-control" />
                <label htmlFor="date">Date</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleTimeChange} value={time} placeholder="time" required type="DATE_AND_TIME" name="time" id="time" className="form-control" />
                <label htmlFor="time">Time</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleTechnicianChange} value={technician} placeholder="technician" required type="text" name="technician" id="technician" className="form-control" />
                <label htmlFor="technician">Technician</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleReasonChange} value={reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                <label htmlFor="reason">Reason</label>
                </div>





                <div className="mb-3">
                {/* <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                    {/* <option value="">Choose a bin</option>
                    {bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.id}>
                                {bin.closet_name}
                            </option>
                        );
                    })} */}
                {/* </select> */}
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );


}


export default AppointmentForm;
