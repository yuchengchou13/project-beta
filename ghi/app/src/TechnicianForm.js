import { React, useEffect, useState } from 'react';


function TechnicianForm() {

    const [name, setName] = useState('');
    const [employee_number, setEmployeeNumber] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.employee_number = employee_number;

        console.log(data);

        const technicianUrl = "http://localhost:3000/technician/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            setName('');
            setEmployeeNumber('');
        }
    }


    const fetchData = async () => {
        const url = "http://localhost:3000/technician/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);




    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a technician</h1>
            <form onSubmit={handleSubmit} id="create-techncian-form">
                <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleEmployeeNumberChange} value={employee_number} placeholder="employee_number" required type="number" name="employee_number" id="employee_number" className="form-control" />
                <label htmlFor="employee_number">Employee Number</label>
                </div>
                <div className="form-floating mb-3">
                {/* <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                    {/* <option value="">Choose a bin</option>
                    {bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.id}>
                                {bin.closet_name}
                            </option>
                        );
                    })} */}
                {/* </select> */}
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );


}


export default TechnicianForm;
