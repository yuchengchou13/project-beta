import { React } from 'react';
import { Link } from 'react-router-dom';


function MainPage(props) {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
      <div className= 'd-grip gap-2 d-sm-flex justify-content-sm-center'>
        <Link to='/salespeople' className='btn btn-primary btn-lg px-4 gap-3'>Add a salesperson</Link>
      </div>
    </div>
  );
}

export default MainPage;
