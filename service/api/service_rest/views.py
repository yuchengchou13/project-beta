from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import ServiceAppointment, Technician

# Create your views here.
# from django.shortcuts import render
# from .models import Appointment

# def appointment_list(request):
#     appointments = Appointment.objects.filter(canceled=False, finished=False)
#     return render(request, 'appointment_list.html', {'appointments': appointments})

class ServiceAppointmentListEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "vin",
        "owner",
        "date",
        "time",
        "technician",
        "reason",
        "purchased_from_dealership",
        "canceled",
        "finished",
    ]

    # econders = {
    #     "something": somethingVOEncoder(),
    # }

    # def get_extra_data(self, o):
    #     return {"something": o.bin.owner.vin.technician} #whatever you want to appear

class ServiceAppointmentDetailEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "vin",
        "owner",
        "date",
        "time",
        "technician",
        "reason",
        "purchased_from_dealership",
        "canceled",
        "finished",
    ]

    # encoders = {
    #     "something": somethingVOEncoder(),
    # }

    # def get_extra_data(self, o):
    #     return {"something": o.bin.owner.vin.technician} #whatever you want to appear


@require_http_methods(["GET", "POST"])
def api_service_list(request):
    if request.method == "GET":
        services = ServiceAppointment.objects.all()
        return JsonResponse(
            {"services": services},
            encoder = ServiceAppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        service = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            service,
            encoder=ServiceAppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_service_detail(request, id):
    if request.method == "GET":
        try:
            services = ServiceAppointment.objects.get(id=id)
            return JsonResponse(
                services,
                encoder=ServiceAppointmentDetailEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            services = ServiceAppointment.objects.get(id=id)
            services.delete()
            return JsonResponse(
                services,
                encoder=ServiceAppointmentDetailEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})



@require_http_methods(["GET", "POST"])
def api_appointment_list(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder = ServiceAppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentListEncoder,
            safe=False,
        )
