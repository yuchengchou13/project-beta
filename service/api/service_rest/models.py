from django.db import models




class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    owner = models.CharField(max_length=50)
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)
    technician = models.CharField(max_length=50)
    reason = models.CharField(max_length=25)
    purchased_from_dealership = models.BooleanField(default=False)
    canceled = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)


    def __str__(self):
        return f"{self.owner}'s Appointment"

    def cancel_appointment(self):
        self.canceled = True
        self.save()

    def finish_appointment(self):
        self.finished=True
        self.save()


class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.IntegerField()

    appointment = models.ForeignKey(
        ServiceAppointment,
        related_name="appointments",
        on_delete=models.CASCADE,
    )


    def __str__(self):
        return self.name
