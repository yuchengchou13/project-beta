# Generated by Django 4.0.3 on 2023-03-07 20:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceAppointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vin', models.CharField(max_length=17, unique=True)),
                ('name', models.CharField(max_length=50)),
                ('date_and_time', models.DateTimeField(auto_now=True)),
                ('technician', models.CharField(max_length=50)),
                ('reason', models.CharField(max_length=25)),
                ('purchased_from_dealership', models.BooleanField(default=False)),
                ('canceled', models.BooleanField(default=False)),
                ('finished', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Technician',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('employee_number', models.IntegerField(max_length=10)),
                ('appointment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='appointments', to='service_rest.serviceappointment')),
            ],
        ),
    ]
