from django.urls import path
from .views import api_service_list, api_service_detail, api_appointment_list

urlpatterns = [
    path("service/", api_service_list , name="api_service_list"),
    path("service/<int:id>/", api_service_detail, name="api_detail"),
    path("appointments/", api_appointment_list, name="api_appointment_list"),
]
