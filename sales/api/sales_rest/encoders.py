from common.json import ModelEncoder
from .models import Customer, salesPerson, Sale , AutomobileVO

class salePersonEncoder(ModelEncoder):
    model = salesPerson
    properties = [
        "name",
        "employee_number",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "city",
        "state",
        "zip_code",
        "phone_number",
        "id",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "sales_person",
        "customer",
    ]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": salePersonEncoder(),
        "customer": CustomerEncoder(),
    }
