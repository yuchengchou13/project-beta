from django.shortcuts import render
from sales_rest.models import AutomobileVO, Customer, Sale, salesPerson
from django.views.decorators.http import require_http_methods
from .encoders import salePersonEncoder, CustomerEncoder, SaleEncoder
from django.http import JsonResponse
import json



@require_http_methods(["GET", "POST"])
def api_sales_personList(request):
    if request.method == "GET":
        sales_person = salesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=salePersonEncoder,
        )
    else:
        content = json.loads(request.body)
        sales_person = salesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=salePersonEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_sales_person(request, id):
    try:
        sales_person = salesPerson.object.get(id=id)
    except salesPerson.DoesNotExist:
        return JsonResponse(
            {"message": "This sales person does not exist."},
            status=404,
        )
    if request.method == "GET":
        return JsonResponse(
            sales_person,
            encoder=salePersonEncoder,
            safe=False,
        )
    else:
        try:
            sales_person = salesPerson.objects.get(id=id)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=salePersonEncoder,
                safe=False,
            )
        except salesPerson.DoesNotExist:
            return JsonResponse({"message": "Sales person does not exist"})

@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method =="GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_customer(request, id):
    try:
        customer = Customer.objects.get(id=id)
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "That customer does not exist."},
            status=404,
        )
    if request.method == "GET":
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})

@require_http_methods(["GET", "POST"])
def api_sale_list(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin."},
                status=400,
            )
        try:

            sales_person = salesPerson.objects.get(employee_number=content["sales_person"])
            content["sales_person"] = sales_person

        except salesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sales person employee number."},
                status=400,
            )

        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id."},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_sale(request, id):
    try:
        sale = Sale.objects.get(id=id)
    except Sale.DoesNotExist:
        return JsonResponse(
            {"message": "That sale does not exist."},
            status=404,
        )
    if request.method == "GET":
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    else:
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "This sale does not exist"})
