from django.db import models
from django.urls import reverse

class salesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number= models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=2)
    zip_code = models.PositiveIntegerField()
    phone_number = models.CharField(max_length=12)

    def customer_address(self):
        return f"{self.address}, {self.city}, {self.state} {self.zip_code}"


    def __str__(self):
        return self.name

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Sale(models.Model):
    price = models.PositiveIntegerField()
    automobile = models.OneToOneField(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    sales_person = models.ForeignKey(
        salesPerson,
        related_name="sales",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_sale", kwargs={"id": self.id})


    def __str__(self):
        return f"{self.automobile} - {self.customer}"
