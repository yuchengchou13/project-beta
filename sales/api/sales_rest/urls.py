from django.urls import path
from .views import (
    api_sales_person,
    api_sales_personList,
    api_customer_list,
    api_customer,
    api_sale_list,
    api_sale,
    )

urlpatterns = [
    path('salespeople/', api_sales_personList, name="api_sales_personList"),
    path('salespeople/<int:id>/', api_sales_person, name="api_sales_person"),
    path('customers/', api_customer_list, name="api_customer_list"),
    path('customer/<int:id>/', api_customer, name="api_customer"),
    path('sales/', api_sale_list, name="api_sale_list"),
    path('sales/<int:id>/', api_sale, name="api_sale"),
]
